const express = require('express');
const app = express();
app.use(express.json());


// const cors = require('cors')

//สร้าง database
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/codehubthailand', { useNewUrlParser: true });
const User = require('./schema')
// app.use(cors())

const students = [
    { id: 1, name: "pipat" },
    { id: 2, name: "naruto" },
    { id: 3, name: "ronaldo" }
];

// เรียกดูข้อมูลด้วย HTTP Method แบบ Get
app.get('/api/students', function (req, res) {
    res.send(students);
});

app.get('/api/students/:id', function (req, res) {
    const id = req.params.id;
    if (id == 1) {
        res.send(students[0]);
    } else if (id == 2) {
        res.send(students[1]);
    } else {
        res.send('Error 404 not found');
    }
});

//เพิ่มข้อมูลด้วย HTTP Method แบบ Post
app.post('/api/add', function (req, res) {
    const studentName = req.body.name;
    if (studentName.length <= 2) {
        res.status(400).send('Error can not add student!');
    } else {
        const student = {
            id: students.length + 1,
            'name': studentName
        }
        students.push(student);
        res.send(student);
    }
});

//อัพเดตข้อมูลด้วย HTTP Method แบบ Put
app.put('/api/update/:id', function (req, res) {
    const id = req.params.id;
    const name = req.body.name;
    if (name.length <= 2 || isNaN(id)) {
        res.status(400).send('Error can not update student!');
    } else {
        const student = students.find(i => i.id === parseInt(id));
        if (student) {
            student.name = name;
            res.send(students);
        } else {
            res.status(400).send('Cannot find student to update');
        }
    }
});

//ลบข้อมูลด้วย method Delete
app.delete('/api/delete/:id', function (req, res) {

    const id = req.params.id;

    const confirmId = req.body.confirmId;

    const student = students.find(i => i.id === parseInt(id));
    console.log(student)
    if (student && confirmId === parseInt(id)) {
        
        const index = students.indexOf(student);
        
        console.log(index)

        students.splice(index, 1);

        res.send('Delete student ' + student.name + ' successfully!');

    } else {

        res.status(400).send('Error cannot delete student!');

    }

});


//เพิ่มข้อมูลลง database
app.post('/post', (req, res) => {

    let userData = req.body
    let user = User(userData)

    user.save((error) => {
        if (error) {
            console.log(error)
        } else {
            res.status(200).send(user)
        }
    })

});


//ดูข้อมูลทั้งหมดจาก database
app.get('/get', (req, res) => {

    User.find({}, (err, user) => {
             res.json(user);
    })
 
        
})

//ดูข้อมูลด้วย id จาก database
app.get('/get/:id', (req, res) => {

    const id = req.params.id

    User.find({id:id}, (err, user) => {
        res.json(user);
    })
        
})

//อัพเดตข้อมูลจาก id ใน database
app.put('/put/:id', (req, res) => {

    const id = req.params.id
    const name = req.body.name

    User.updateOne({id:id},{$set: {name:name}}, (err, user) => {
        res.json(user);
    })
})
//ลบข้อมูลจาก id ใน database
app.delete('/delete/:id', (req, res) => {

    const id = req.params.id

    User.deleteOne({id:id}, (err, user) => {
        res.json(user);
    })
})



const port = process.env.PORT || 3000;
app.listen(port, function () {
    console.log('Listening on port', port);
});
