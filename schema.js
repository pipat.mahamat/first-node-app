const mongoose = require('mongoose');

const Schema = mongoose.Schema
const FeedbackSchema = new Schema({
    id: { type: String},
    name: { type: String}

})
const FeedbackModel = mongoose.model('student', FeedbackSchema);

module.exports = FeedbackModel;
